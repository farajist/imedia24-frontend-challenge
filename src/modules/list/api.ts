import { AxiosRequestConfig, AxiosResponse } from 'axios';
import request from '../../utils/request';
import { PokemonListResponse } from './domain';

export const getPokemons = (
  config?: AxiosRequestConfig
): Promise<AxiosResponse<PokemonListResponse>> =>
  request.get('/api/v2/pokemon', config);
