export interface PokemonItem {
  name: string;
  url: string;
}

export interface PokemonListRequestConfig {
  params: string;
}

export interface PokemonListResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<PokemonItem>;
}
