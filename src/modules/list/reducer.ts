import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../store/store';
import { fromObject, fromURL } from '../../utils/qs';
import { PokemonItem, PokemonListResponse } from './domain';

export interface ListState {
  pokemons: PokemonItem[];
  paginationParams: string | null;
  count: number;
  loading: boolean;
  errors: any;
}

const initialState: ListState = {
  pokemons: [],
  paginationParams: fromObject({
    limit: '100',
    offset: '0'
  }),
  count: 0,
  loading: false,
  errors: []
};

export const listSlice = createSlice({
  name: 'list',
  initialState,
  reducers: {
    fetchPokemonListAction: (state) => {
      state.loading = true;
      state.errors = [];
    },
    fetchPokemonListSuccessAction: (
      state,
      action: PayloadAction<PokemonListResponse>
    ) => {
      state.loading = false;
      state.paginationParams = fromURL(action.payload.next);
      state.pokemons = [...state.pokemons, ...action.payload.results];
      state.count = action.payload.count;
    },
    fetchPokemonListFailedAction: (state, action: PayloadAction<any>) => {
      state.loading = false;
      state.errors = action.payload.toString();
    },
    fetchPokemonListEndAction: (state) => {
      state.loading = false;
    }
  }
});

export const selectPokemons = (state: RootState) => state.list.pokemons;
export const selectPaginationParams = (state: RootState) =>
  state.list.paginationParams;

export const selectHasNext = (state: RootState) =>
  !!state.list.paginationParams;

export const selectLoading = (state: RootState) => state.list.loading;

export const {
  fetchPokemonListAction,
  fetchPokemonListFailedAction,
  fetchPokemonListSuccessAction,
  fetchPokemonListEndAction
} = listSlice.actions;

export default listSlice.reducer;
