import { PayloadAction } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import {
  call,
  CallEffect,
  put,
  PutEffect,
  SagaReturnType,
  select,
  SelectEffect,
  takeLatest
} from 'redux-saga/effects';
import { toSearchParams } from '../../utils/qs';
import { getPokemons } from './api';
import { PokemonListResponse } from './domain';
import {
  fetchPokemonListAction,
  fetchPokemonListEndAction,
  fetchPokemonListFailedAction,
  fetchPokemonListSuccessAction,
  selectHasNext,
  selectPaginationParams
} from './reducer';

type PokemonListResponseType = SagaReturnType<typeof getPokemons>;

function* fetchPokemonList(): Generator<
  | SelectEffect
  | CallEffect<PokemonListResponseType>
  | PutEffect<PayloadAction<PokemonListResponse>>
  | PutEffect<PayloadAction>,
  void,
  never
> {
  const qs = (yield select(selectPaginationParams)) as string;
  const hasNext = (yield select(selectHasNext)) as boolean;
  if (!hasNext) {
    yield put(fetchPokemonListEndAction());
    return;
  }
  try {
    const pokemonData: AxiosResponse = yield call(getPokemons, {
      params: toSearchParams(qs)
    });
    yield put(fetchPokemonListSuccessAction(pokemonData.data));
  } catch (error) {
    yield put(fetchPokemonListFailedAction(error));
  }
}

function* fetchPokemonListSaga() {
  yield takeLatest(fetchPokemonListAction, fetchPokemonList);
}

export const listSagas = [fetchPokemonListSaga];
