import { useEffect } from 'react';
import PokemonListItem from './components/PokemonListItem';
import Spinner from '../../components/Spinner';
import { useAppDispatch, useAppSelector } from '../../store';
import {
  fetchPokemonListAction,
  selectHasNext,
  selectLoading,
  selectPokemons
} from './reducer';
import { PokemonItem } from './domain';

interface PokemonListProps {}

const PokemonList: React.FC<PokemonListProps> = () => {
  const pokemons = useAppSelector(selectPokemons);
  const hasNext = useAppSelector(selectHasNext);
  const loading = useAppSelector(selectLoading);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchPokemonListAction());
  }, []);

  function handleScroll() {
    const { scrollHeight, scrollTop, clientHeight } = document.documentElement;
    const isAtBottom = scrollHeight - scrollTop <= clientHeight;
    if (isAtBottom && hasNext) dispatch(fetchPokemonListAction());
  }

  window.addEventListener('scroll', handleScroll);
  return (
    <div
      data-testid="pokemon-list"
      className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8"
    >
      <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
        Pokemon List
      </h2>
      <div className="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2">
        {pokemons.map((pokemon: PokemonItem) => (
          <PokemonListItem pokemon={pokemon} key={pokemon.url} />
        ))}
      </div>
      {loading && (
        <div
          data-testid="loading-container"
          role="status"
          className="mt-8 flex justify-center items-center"
        >
          <Spinner />
          <span className="sr-only">Loading...</span>
        </div>
      )}
    </div>
  );
};

export default PokemonList;
