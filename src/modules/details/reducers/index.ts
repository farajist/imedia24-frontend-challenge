import { createSlice } from '@reduxjs/toolkit';

export interface DetailsState {}

export const detailsSlice = createSlice({
  name: 'details',
  initialState: [],
  reducers: {}
});

export default detailsSlice.reducer;
