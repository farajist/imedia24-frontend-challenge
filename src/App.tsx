import PokemonList from './modules/list';

function App() {
  return (
    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <PokemonList />
    </div>
  );
}

export default App;
