interface Env {
  apiUrl: string;
}
function getEnv(): Env {
  return {
    apiUrl: process.env.REACT_APP_API_URL as string
  };
}
const env = getEnv();
export default env;
