import axios from 'axios';
import env from '../environment';

const instance = axios.create({
  baseURL: env.apiUrl
});

instance.interceptors.request.use((request) => {
  console.debug('[Axios/Request] Sending : \n', request);
  return request;
});

instance.interceptors.response.use((response) => {
  console.debug('[Axios/Response] Receiving : \n', response);
  return response;
});

export default instance;
