export const fromURL = (url: string | null) =>
  url ? new URLSearchParams(new URL(url).search).toString() : null;

export const fromObject = (obj: Record<string, string>) =>
  new URLSearchParams(obj).toString();

export const toSearchParams = (qs: string) => new URLSearchParams(qs);
