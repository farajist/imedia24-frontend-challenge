import { detailsSagas } from '../modules/details/sagas';
import { listSagas } from '../modules/list/saga';

export const appSagas = [...listSagas, ...detailsSagas];
