import createSagaMiddleware from '@redux-saga/core';
import {
  applyMiddleware,
  configureStore,
  PreloadedState
} from '@reduxjs/toolkit';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import { appSagas } from './sagas';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger({ timestamp: true, duration: true });

export const createStore = (preloadedState: PreloadedState<RootState>) => {
  const middleware = [sagaMiddleware, loggerMiddleware];
  const enhancers = [composeWithDevTools(applyMiddleware(...middleware))];
  const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({ thunk: false }).concat(...middleware),
    enhancers,
    devTools: false
  });

  if (Array.isArray(appSagas)) {
    appSagas.forEach((saga) => sagaMiddleware.run(saga));
  }

  return store;
};

const store = createStore({});

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof createStore>;
export type AppDispatch = typeof store.dispatch;

export default store;
