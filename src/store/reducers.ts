import { combineReducers } from '@reduxjs/toolkit';
import detailsReducer from '../modules/details/reducers';
import listReducer from '../modules/list/reducer';

const rootReducer = combineReducers({
  list: listReducer,
  details: detailsReducer
});

export default rootReducer;
