import { screen } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import PokemonList from '../../../modules/list';
import { fromObject } from '../../../utils/qs';
import { renderWithProviders } from '../../../utils/test';

const mockState = {
  list: {
    pokemons: [],
    paginationParams: fromObject({ limit: '100', offset: '0' }),
    count: 1,
    loading: false,
    errors: undefined
  }
};
export const handlers = [
  rest.get('https://pokeapi.co/api/v2/pokemon', (req, res, ctx) => {
    return res(
      ctx.json({ results: [{ name: 'Oddish', url: 'pokemon/oddish' }] })
    );
  })
];

const server = setupServer(...handlers);

beforeAll(() => server.listen());

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test('should display given pokemons lists', async () => {
  renderWithProviders(<PokemonList />, {
    preloadedState: mockState
  });

  expect(screen.getByTestId('loading-container')).toBeInTheDocument();

  // after some time, the user should be received
  expect(await screen.findByText(/Oddish/i)).toBeInTheDocument();
});
