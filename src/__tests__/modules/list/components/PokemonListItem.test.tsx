import { render, screen } from '@testing-library/react';
import { PokemonItem } from '../../../../modules/list/domain';
import ListItem from '../../../../modules/list/components/PokemonListItem';

test('it displays correct pokemon name', () => {
  const oddish: PokemonItem = { name: 'oddish', url: 'pokemon/oddish' };
  render(<ListItem pokemon={oddish} />);

  const pokemonName = screen.getByTestId('pokemon-item-name');
  expect(pokemonName).toHaveTextContent('Oddish');
});
