# iMedia24 Front-end coding challenge: Pokemons

## Introduction

This is an implementation of iMedia24 frontend challenge using React/Typescript/Redux, the application fetches the list of pokemon using [PokeAPI](https://pokeapi.co/)
with an infinite scroll feature in place.

## Development

The application is based on CRA, to run:

```shell
# to install dependencies
yarn
# start the dev server
yarn start
```

## Testing

To run unit tests

```
yarn test
```

## Roadmap

- [x] Setup project starter (dependecies, basic redux/saga skeleton, css library)
- [x] Implement infinite scroll list
- [x] Write unit tests for list feature
- [x] Write documentation (README.md)
- [ ] Implement E2E tests (using Cypress)
- [ ] Implement details feature
